import { Injectable } from '@angular/core';
import { HttpModule, Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Configuration } from '../app.constant';

@Injectable()
export class PromotionPlanService {
    public Url: string;

    constructor(public http: Http, public _config: Configuration) { }

    getPromotionPlans(): Observable<any> {
        let url = this._config.Server + "promotionPlans";
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    submitPlan(data): Observable<any> {
        let url = this._config.Server + "promotionPlans";
        let body = JSON.stringify(data);
        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());
    }
    gotoPlanEditData(data): Observable<any> {
        let url = this._config.Server + "promotionPlans";
        let body = JSON.stringify(data);
        return this.http.put(url, body, { headers: this._config.headers }).map(res => res.json());
    }
    gotoDeletePlans(planId): Observable<any> {
        let url = this._config.Server + "promotionPlans/" + planId;
        return this.http.delete(url, { headers: this._config.headers }).map(res => res.json());

    }


}