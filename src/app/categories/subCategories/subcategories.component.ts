import { Component, OnInit, OnDestroy, ViewEncapsulation, ViewContainerRef, Input, AfterViewInit, AfterContentInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Modal } from 'angular2-modal/plugins/bootstrap';
import { ModalModule } from "ng2-modal";
import { SubcategoriesService } from './subcategories.service';
import { Location } from '@angular/common';
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary'
import { PagesComponent } from '../../pages/pages.component';

declare var swal: any;
declare var sweetAlert: any;
declare var CLOUDNAME: string, CLOUDPRESET: string;

@Component({
    selector: 'subcategories',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./subcategories.component.scss'],
    templateUrl: './subcategories.component.html',
    providers: [SubcategoriesService]

})

export class SubcategoriesComponent implements OnInit {
    sub: any;
    categoriesName: any;
    msg: any = false;
    subCategory: any;
    public rowsOnPage = 10;
    addSubCategory: FormGroup;
    editSubCategory: FormGroup;
    public obj = '';
    addFields: any;
    editFields = [];
    oldName: any;
    newName: any;
    public url: any;
    logoUrl: any;
    state = 1;
    errorImg = false;
    oldSubcat: any;
    newSubcat: any;
    langData = [];
    image: any = '';
    oldImage: any = '';
    btnFlag = true;

    cloudinaryImage: any;
    uploader: CloudinaryUploader = new CloudinaryUploader(
        new CloudinaryOptions({ cloudName: CLOUDNAME, uploadPreset: CLOUDPRESET })
    );



    constructor(private route: ActivatedRoute, private _subcategoriesService: SubcategoriesService, private router: Router, vcRef: ViewContainerRef, fb: FormBuilder, public _isAdmin: PagesComponent) {
        this.addSubCategory = fb.group({
            'subCategoryName': ['', Validators.required],
        });
        this.editSubCategory = fb.group({
            'subCategoryName': ['', Validators.required],
        });

    }
    upload() {
        this.uploader.uploadAll();
        let photo = jQuery("#photo").val();
        if (photo) {
            this.errorImg = false;
            this.uploader.uploadAll();
        } else {
            this.errorImg = true;
        }
    }
    ngOnInit() {
        if (this._isAdmin.isAdmin == false) {
            var role = sessionStorage.getItem('role');
            var roleDt = JSON.parse(role);
            for (var x in roleDt) {
                if (x == 'categories') {
                    if (roleDt[x] == 0) {
                        this.router.navigate(['error']);
                    } else if (roleDt[x] == 100) {
                        jQuery(".buttonCus").hide();
                        jQuery(".thAction").remove();
                        // jQuery(".thTitle").css("width", "61%");
                        this.btnFlag = false;
                    } else if (roleDt[x] == 110) {
                        this.btnFlag = false;
                        jQuery(".thAction").remove();
                        // jQuery(".thTitle").css("width", "61%");
                    }
                }
            }
        }



        jQuery("#upImg").hide();
        jQuery("#upImge").hide();
        this.sub = this.route.params.subscribe(params => {
            this.categoriesName = params['categoryName'];

        });

        this._subcategoriesService.getSubcategory(this.categoriesName).subscribe(
            result => {

                console.log("res", result);
                if (result.data && result.data.length > 0) {
                    this.subCategory = result.data;
                    this.msg = false;
                    //console.log("list data",this.subCategory);
                } else {
                    this.subCategory = [];
                    this.msg = true;
                }
            }
        )

        this._subcategoriesService.getLanguage().subscribe(
            res => {
                // console.log("res", res);
                if (res.data && res.data.length > 0) {
                    this.langData = res.data;
                } else {
                    this.langData = [];
                }
            }
        )
    }

    submitForm(value) {
        // console.log(value._value)
        // console.log("image", this.image);
        var otherName = [];
        this.langData.forEach(e => {
            let x = {
                langId: e.langId,
                name: jQuery(".addSub" + e.langId).val()
            }
            otherName.push(x);
        });
        value._value.image = this.image;
        value._value.otherName = otherName;
        this._subcategoriesService.addSubCategory(value._value, this.categoriesName).subscribe(
            result => {
                console.log("result", result);
                if (result.code == 200) {
                    swal("Success!", "SubCategory Added Successfully!", "success");
                    jQuery('#addSubCategory').modal('hide');
                    this.image = '';
                    this.ngOnInit();
                    this.addSubCategory.reset();
                } else {
                    sweetAlert("Oops...", "Something went wrong!", "error");
                }
            }
        )
    }

    gotoeditSubcategory(subcat) {
        jQuery(".subXyz").val('');
        this.image = subcat.imageUrl;
        this.oldImage = subcat.imageUrl;
        this.obj = subcat.subCategoryName;
        this.oldSubcat = subcat.subCategoryName;
        jQuery('#newSubcat').val(this.oldSubcat);
        this.langData.forEach(ele => {
            subcat.subOtherName.forEach(element => {
                if (ele.langId == element.langId) {
                    jQuery(".editSub" + ele.langId).val(element.otherName);
                }
            });
        });


    }
    fileChange(input) {
        if ((input.files[0].size / 1000) > 300) {
            jQuery("#upImg").show();
        } else {
            jQuery("#upImg").hide();
            const reader = new FileReader();
            if (input.files.length) {
                const file = input.files[0];
                reader.onload = () => {
                    this.image = reader.result;
                }
                reader.readAsDataURL(file);
            }
        }
    }
    removeImage(): void { this.image = ''; }
    updatecategory(subcat) {
        // console.log(subcat);
        this.newSubcat = jQuery("input#newSubcat").val();
        var otherName = [];
        this.langData.forEach(e => {
            let x = {
                langId: e.langId,
                name: jQuery(".editSub" + e.langId).val()
            }
            otherName.push(x);
        });

        this._subcategoriesService.editFields(this.newSubcat, this.oldSubcat, this.categoriesName, otherName, this.image, this.oldImage).subscribe(
            result => {
                if (result.data && result.data.length > 0) {
                    jQuery('#editSubCategory').modal('hide');
                    this.ngOnInit();
                    swal("Good job!", "SubCategory Updated Successfully!", "success");
                } else {
                    sweetAlert("Oops...", "Something went wrong!", "error");
                }
            }
        )
    }
    gotoDeleteSubcate(subcategory, image) {
        //console.log("u wana delete id is:-",subcategory);
        var cat = this;
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this SubCategory!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
            function (isConfirm) {
                if (isConfirm) {
                    cat.deletesubcategory(subcategory, image);
                    swal({
                        title: 'Delete!',
                        text: 'SubCategory Deleted Successfully!',
                        type: 'success'
                    });

                } else {
                    swal("Cancelled", "Your Sub Category is safe :)", "error");
                }
            });
    }
    deletesubcategory(subcategory, image) {
        this._subcategoriesService.deletesubcategory(subcategory, this.categoriesName, image).subscribe(
            result => {
                //console.log("delete result", result);
                this.ngOnInit();

            }
        )
    }

    gotoSub(categoriesName: any) {
        this.router.navigate(['/app/categories/sub-categories', categoriesName]);
    }
    gotoField(subCategoryName: any, categoriesName: any) {
        this.router.navigate(['/app/categories/sub-categories/field', subCategoryName]);
    }

    gotoUpArrow(index) {
        if (index > 0) {
            const tmp = this.subCategory[index - 1];
            this.subCategory[index - 1] = this.subCategory[index];
            this.subCategory[index] = tmp;
            this._subcategoriesService.gotoRowsUpate(this.subCategory[index].subCatId, this.subCategory[index - 1].subCatId, 2).subscribe(
                res => {
                    this.ngOnInit();
                }
            )
        }
    }
    gotoDownArrow(index) {
        if (index < this.subCategory.length) {
            const tmp = this.subCategory[index + 1];
            this.subCategory[index + 1] = this.subCategory[index];
            this.subCategory[index] = tmp;
            this._subcategoriesService.gotoRowsUpate(this.subCategory[index].subCatId, this.subCategory[index + 1].subCatId, 2).subscribe(
                res => {
                    this.ngOnInit();
                }
            )
        }
    }
}